﻿using System.Data.Entity;
using System.Linq;
using ClaimDesk.Engine.Domains;

namespace ClaimDesk.Engine.Data
{
	public class EfRepository<T> : IRepository<T> where T : BaseDomain
	{
		private readonly IDbSet<T> _dbSet;
		private DbContext _dbContext;

		public EfRepository(DbContext dbContext)
		{
			_dbContext = dbContext;
			_dbSet = _dbContext.Set<T>();
		}

		public IQueryable<T> Table()
			=> _dbSet.AsNoTracking();

		public void Add(T domain)
		{
			_dbSet.Add(domain);
			_dbContext.SaveChanges();
		}

		public void Update(T domain)
		{
			_dbContext.Entry(domain).State = EntityState.Modified;
			_dbContext.SaveChanges();
		}

		public void Delete(int id)
		{
			var domain = Get(id);

			if (domain == null)
				return;

			_dbSet.Remove(domain);
			_dbContext.SaveChanges();
		}

		public T Get(int id)
			=> _dbSet.Find(id);

		public void Dispose()
		{
			if (_dbContext == null)
				return;

			_dbContext.Dispose();
			_dbContext = null;
		}
	}
}