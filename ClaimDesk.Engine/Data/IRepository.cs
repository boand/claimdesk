﻿using System;
using System.Linq;
using ClaimDesk.Engine.Domains;

namespace ClaimDesk.Engine.Data
{
	public interface IRepository<T> : IDisposable
		where T : BaseDomain
	{
		T Get(int id);
		IQueryable<T> Table();
		void Add(T domain);
		void Update(T domain);
		void Delete(int id);
	}
}