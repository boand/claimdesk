﻿using System.Data.Entity;
using ClaimDesk.Engine.Domains;

namespace ClaimDesk.Engine.Data
{
	public class ClaimDeskDataContext : DbContext
	{
		public DbSet<ClaimDomain> Claims { get; set; }
		public DbSet<ClaimHistoryDomain> Comments { get; set; }
	}
}