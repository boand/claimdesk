﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ClaimDesk.Engine.Domains
{
	[Table("Claims")]
	public class ClaimDomain : BaseDomain
	{
		[Required]
		public string Description { get; set; }

		public virtual ICollection<ClaimHistoryDomain> History { get; set; } = new List<ClaimHistoryDomain>();
	}
}