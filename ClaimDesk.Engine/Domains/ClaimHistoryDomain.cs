﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using ClaimDesk.Engine.Enums;

namespace ClaimDesk.Engine.Domains
{
	[Table("History")]
	public class ClaimHistoryDomain : BaseDomain
	{
		public DateTime СhangeDate { get; set; } = DateTime.Now;
		public ClaimStatus Status { get; set; }
		public string Reason { get; set; }

		public int ClaimId { get; set; }
		public virtual ClaimDomain Claim { get; set; }
	}
}