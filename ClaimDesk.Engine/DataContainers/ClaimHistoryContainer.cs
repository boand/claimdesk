﻿using ClaimDesk.Engine.Domains;
using ClaimDesk.Engine.Enums;

namespace ClaimDesk.Engine.DataContainers
{
	public class ClaimHistoryContainer
	{
		public string Reason;
		public ClaimStatus Status;
		public string StatusDescription;
		public string СhangeDate;

		public ClaimHistoryContainer(ClaimHistoryDomain domain)
		{
			СhangeDate = domain.СhangeDate.ToString();
			Status = domain.Status;
			StatusDescription = domain.Status.GetDescription();
			Reason = domain.Reason;
		}
	}
}