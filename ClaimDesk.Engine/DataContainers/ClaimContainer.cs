﻿using System;
using System.Linq;
using ClaimDesk.Engine.Domains;
using ClaimDesk.Engine.Enums;

namespace ClaimDesk.Engine.DataContainers
{
	public class ClaimContainer
	{
		public DateTime CreationDate;
		public string Description;
		public int Id;
		public ClaimStatus Status;
		public DateTime? СhangeDate;

		public ClaimContainer(ClaimDomain domain)
		{
			Id = domain.Id;
			CreationDate = domain.History.Single(historyDomain => historyDomain.Status == ClaimStatus.Opened).СhangeDate;
			Description = domain.Description;
			Status = domain.History.Last().Status;
			СhangeDate = domain.History.LastOrDefault(historyDomain => historyDomain.Status != ClaimStatus.Opened)?.СhangeDate;
		}
	}
}