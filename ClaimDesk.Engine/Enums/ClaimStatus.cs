﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace ClaimDesk.Engine.Enums
{
	public enum ClaimStatus
	{
		[Description("Открыта")] Opened,
		[Description("Решена")] Resolved,
		[Description("Возврат")] Return,
		[Description("Закрыта")] Closed
	}

	public static class ClaimStatusExtensions
	{
		public static ClaimStatus[] GetAvailableStatuses(this ClaimStatus currentStatus)
		{
			switch (currentStatus)
			{
				case ClaimStatus.Opened:
					return new[] {ClaimStatus.Resolved};
				case ClaimStatus.Resolved:
					return new[] {ClaimStatus.Return, ClaimStatus.Closed};
				case ClaimStatus.Return:
					return new[] {ClaimStatus.Resolved};
				case ClaimStatus.Closed:
					return new ClaimStatus[0];
				default:
					throw new ArgumentOutOfRangeException(nameof(currentStatus), currentStatus, null);
			}
		}

		public static bool CanChange(this ClaimStatus currentStatus, ClaimStatus newStatus)
			=> currentStatus.GetAvailableStatuses().Contains(newStatus);

		public static string GetDescription(this Enum value)
		{
			var fi = value.GetType().GetField(value.ToString());

			var attribute = fi.GetCustomAttribute(typeof (DescriptionAttribute), false) as DescriptionAttribute;

			return attribute?.Description ?? value.ToString();
		}
	}
}