﻿using System;

namespace ClaimDesk.Engine.Exceptions
{
	public class UnableChangeStatusException : ArgumentException
	{
		public UnableChangeStatusException() : base("Невозможно изменить статус")
		{
		}
	}
}