﻿using System;

namespace ClaimDesk.Engine.Exceptions
{
	public class ClaimNotExistException : ArgumentException
	{
		public ClaimNotExistException(int claimId) : base($"Заявки с ид {claimId} не существует")
		{
		}
	}
}