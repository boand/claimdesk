﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ClaimDesk.Engine.Data;
using ClaimDesk.Engine.DataContainers;
using ClaimDesk.Engine.Domains;
using ClaimDesk.Engine.Enums;
using ClaimDesk.Engine.Exceptions;
using ClaimDesk.Engine.Services.Interfaces;

namespace ClaimDesk.Engine.Services
{
	public class ClaimService : IClaimService
	{
		private readonly IRepository<ClaimDomain> _repository;

		public ClaimService(IRepository<ClaimDomain> repository)
		{
			_repository = repository;
		}

		public IEnumerable<ClaimContainer> GetClaims(DateTime? minDate, DateTime? maxDate, ClaimStatus? status)
		{
			var queryable = _repository.Table().Include(claimDomain => claimDomain.History);

			if (minDate.HasValue)
				queryable = queryable.Where(
					claimDomain => claimDomain.History.OrderByDescending(historyDomain => historyDomain.СhangeDate).FirstOrDefault().СhangeDate >= minDate.Value);
			if (maxDate.HasValue)
				queryable = queryable.Where(
					claimDomain => claimDomain.History.OrderByDescending(historyDomain => historyDomain.СhangeDate).FirstOrDefault().СhangeDate <= maxDate.Value);
			if (status.HasValue)
				queryable = queryable.Where(
					claimDomain => claimDomain.History.OrderByDescending(historyDomain => historyDomain.СhangeDate).FirstOrDefault().Status == status.Value);

			var claims = queryable.OrderByDescending(domain => domain.Id).ToArray().Select(claimDomain => new ClaimContainer(claimDomain));
			return claims;
		}

		private ClaimDomain GetDomainById(int claimId)
		{
			var claimDomain = _repository.Get(claimId);

			if (claimDomain == null)
				throw new ClaimNotExistException(claimId);
			return claimDomain;
		}

		public IEnumerable<ClaimHistoryContainer> GetClaimHistory(int claimId)
			=> GetDomainById(claimId).History.Select(historyDomain => new ClaimHistoryContainer(historyDomain));

		public void SaveClaim(int? claimId, string description)
		{
			if (!claimId.HasValue)
			{
				var domain = new ClaimDomain {Description = description};
				domain.History.Add(new ClaimHistoryDomain {Status = ClaimStatus.Opened, Reason = "Создание заявки"});
				_repository.Add(domain);
			}
			else
			{
				var domain = GetDomainById(claimId.Value);
				domain.Description = description;
				_repository.Update(domain);
			}
		}

		public void SaveClaim(int claimId, ClaimStatus status, string reason)
		{
			var domain = GetDomainById(claimId);

			if (!domain.History.Last().Status.CanChange(status))
				throw new UnableChangeStatusException();

			domain.History.Add(new ClaimHistoryDomain {Status = status, Reason = reason});
			_repository.Update(domain);
		}

		public void DeleteClaim(int claimId)
			=> _repository.Delete(claimId);
	}
}