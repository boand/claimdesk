﻿using System;
using System.Collections.Generic;
using ClaimDesk.Engine.DataContainers;
using ClaimDesk.Engine.Enums;

namespace ClaimDesk.Engine.Services.Interfaces
{
	public interface IClaimService
	{
		IEnumerable<ClaimContainer> GetClaims(DateTime? minDate, DateTime? maxDate, ClaimStatus? status);
		IEnumerable<ClaimHistoryContainer> GetClaimHistory(int claimId);
		void SaveClaim(int? claimId, string description);
		void SaveClaim(int claimId, ClaimStatus status, string reason);
		void DeleteClaim(int claimId);
	}
}