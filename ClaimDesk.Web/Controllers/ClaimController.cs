﻿using System.Linq;
using System.Web.Mvc;
using ClaimDesk.Engine.Data;
using ClaimDesk.Engine.Domains;
using ClaimDesk.Engine.Enums;
using ClaimDesk.Engine.Services;
using ClaimDesk.Engine.Services.Interfaces;
using ClaimDesk.Web.Models.Claim;

namespace ClaimDesk.Web.Controllers
{
	public class ClaimController : Controller
	{
		private readonly IClaimService _claimService;
		private readonly IRepository<ClaimDomain> _repository = new EfRepository<ClaimDomain>(new ClaimDeskDataContext());

		public ClaimController()
		{
			_claimService = new ClaimService(_repository);
		}

		public ActionResult Index(IndexClaimViewModel model)
		{
			model.Claims = _claimService.GetClaims(model.MinDate, model.MaxDate, model.Status);
			return View(model);
		}

		public ActionResult GetClaimHistory(int claimId)
		{
			var history = _claimService.GetClaimHistory(claimId).ToArray();
			var statuses = history.Last().Status.GetAvailableStatuses().Select(status => new {text = status.GetDescription(), value = status});
			return Json(new {statuses, history}, JsonRequestBehavior.AllowGet);
		}

		public ActionResult Save(SaveClaimViewModel model)
		{
			_claimService.SaveClaim(model.Id, model.Description);
			return RedirectToAction("Index");
		}

		public ActionResult ChangeStatus(ChangeStatusClaimViewModel model)
		{
			_claimService.SaveClaim(model.ClaimId, model.NewStatus, model.Reason);
			return RedirectToAction("Index");
		}

		public ActionResult Delete(int id)
		{
			_claimService.DeleteClaim(id);
			return RedirectToAction("Index");
		}

		protected override void Dispose(bool disposing)
		{
			_repository.Dispose();
			base.Dispose(disposing);
		}
	}
}