﻿using System.Web.Mvc;
using System.Web.Routing;

namespace ClaimDesk.Web
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.MapRoute("Default", "{controller}/{action}/{id}", new {controller = "Claim", action = "Index", id = UrlParameter.Optional});
		}
	}
}