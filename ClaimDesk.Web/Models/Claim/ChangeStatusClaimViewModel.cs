﻿using ClaimDesk.Engine.Enums;

namespace ClaimDesk.Web.Models.Claim
{
	public class ChangeStatusClaimViewModel
	{
		public int ClaimId { get; set; }
		public ClaimStatus NewStatus { get; set; }
		public string Reason { get; set; }
	}
}