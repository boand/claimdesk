﻿using System;
using System.Collections.Generic;
using ClaimDesk.Engine.DataContainers;
using ClaimDesk.Engine.Enums;

namespace ClaimDesk.Web.Models.Claim
{
	public class IndexClaimViewModel
	{
		public string Title = "Список заявок";
		public DateTime? MinDate { get; set; }
		public DateTime? MaxDate { get; set; }
		public ClaimStatus? Status { get; set; }
		public IEnumerable<ClaimContainer> Claims { get; set; }
	}
}