﻿namespace ClaimDesk.Web.Models.Claim
{
	public class SaveClaimViewModel
	{
		public int? Id { get; set; }
		public string Description { get; set; }
	}
}